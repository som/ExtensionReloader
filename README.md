# Extension Reloader

A developer tool to effectively reload a GNOME Shell extension.

## Usage

* Main.extensionManager.trulyReloadExtension:

```js
try {
    let [newUuid, openExtensionPrefsFunction] = Main.extensionManager.trulyReloadExtension(uuid);
    log(`Now the extension uuid is ${newUuid}`);

    // A convenient function to open the preferences quickly.
    openExtensionPrefsFunction();
} catch(e) {
    log(e.message);
}
```

* Main.extensionManager.trulyReloadExtensionAsync variant:

```js
Main.extensionManager.trulyReloadExtensionAsync('drawOnYourScreen@som.codeberg.org')
    .then((newUuid, openExtensionPrefsFunction) => {
        log(`Now the extension uuid is ${newUuid}`);
        
        // A convenient function to open the preferences quickly.
        openExtensionPrefsFunction();
    })
    .catch(logError);
```

* DBus service reachable, for example, from your editor:

```sh
dbus-send --print-reply --dest=org.gnome.Shell \
          /org/codeberg/som/ExtensionReloader \
          org.codeberg.som.ExtensionReloader.ReloadExtension \
          string:'<uuid>'
```

It will return the success, the new uuid and and an error message.

## Make your extension compatible

* Expect the uuid to be changing:

  ~~`const uuid = 'myExtension@mydomain';`~~  
  `const uuid = imports.misc.extensionUtils.getCurrentExtension().uuid;`

* If you name your GType, GObject classes or Lang classes, do this against the extension uuid in order to avoid collisions:

```js
const uuid = imports.misc.extensionUtils.getCurrentExtension().uuid;
const sanitize = s => s.replace(/[^a-z0-9+_-]/gi, '_'); // Useless since GS 3.36

const MyObject = new GObject.Class({
    Name: sanitize(`${uuid}.MyObject`),
    GTypeName: sanitize(`Gjs_${uuid}.MyObject`),

    _init: ...
});

const MyClass = new Lang.Class({
    Name: sanitize(`${uuid}.MyClass`),

    _init: ...
});
```

* And of course be careful of the `disable` stuff.

## Note

After a shell restart, some uuids ending with '-reloaded${number}' (maximum one per extension) may remain in the `/org/gnome/shell/enabled-extensions` setting list. It has absolutely no consequences but you can safely remove them as long as you do not have reloaded any extension during the current shell session.

## UI

* Make your own.
* Have a look at [ExtensionReloaderButton](https://codeberg.org/som/ExtensionReloaderButton).

## Under the hood

GNOME Shell has already in its own a function to reload extensions (`Main.extensionMAnager.reloadExtension`). In most cases, it is inefficient.

> If we did install an importer, it is now cached and it's impossible to load a different version

In other words, GJS cannot update your script files.

So the workaround is very stupid:

* Unload the current extension.
* Duplicate the extension directory in /tmp.
* Change the uuid in the new location (.i.e. the directory name and the value of the `uuid` key in `metadata.json`).
* Load the extension with the new location and the new uuid.

It is completely transparent.

## One last little detail

**IT WORKS WITH WAYLAND**.

/*
 * Copyright 2020 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2020 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* jslint esversion: 8 */
/* exported init */

const ByteArray = imports.byteArray;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;

const ExtensionDownloader = imports.ui.extensionDownloader;
const ExtensionUtils = imports.misc.extensionUtils;
const FileUtils = imports.misc.fileUtils;
const Main = imports.ui.main;

const ENABLED_EXTENSIONS_KEY = 'enabled-extensions';
const DISABLED_EXTENSIONS_KEY = 'disabled-extensions';
const MATCH = '%s-reloaded';
const UUID = '%s-reloaded%d';
const DBUS_OBJECT_PATH = '/org/codeberg/som/ExtensionReloader';
const DBUS_INTERFACE = `
<node>
  <interface name="org.codeberg.som.ExtensionReloader">
    <method name="ReloadExtension">
      <arg type="s" direction="in" name="uuid"/>
      <arg type="b" direction="out" name="success"/>
      <arg type="s" direction="out" name="new uuid"/>
      <arg type="s" direction="out" name="error"/>
    </method>
  </interface>
</node>`;

function recursivelyCopyDir(srcDir, destDir) {
    let children = srcDir.enumerate_children('standard::name,standard::type',
                                             Gio.FileQueryInfoFlags.NONE, null);

    if (!destDir.query_exists(null))
        destDir.make_directory_with_parents(null);

    let info;
    while ((info = children.next_file(null)) != null) {
        if (info.get_name() == '.git')
            continue;

        let type = info.get_file_type();
        let srcChild = srcDir.get_child(info.get_name());
        let destChild = destDir.get_child(info.get_name());
        if (type == Gio.FileType.REGULAR)
            srcChild.copy(destChild, Gio.FileCopyFlags.NONE, null, null);
        else if (type == Gio.FileType.DIRECTORY)
            recursivelyCopyDir(srcChild, destChild);
    }
}

function changeMetadataUuid(newDir, newUuid) {
    let metadataFile = newDir.get_child('metadata.json');
    if (!metadataFile.query_exists(null))
        throw new Error('Missing metadata.json');

    let metadataContents, success_;
    try {
        [success_, metadataContents] = metadataFile.load_contents(null);
        metadataContents = ByteArray.toString(metadataContents);
    } catch (e) {
        throw new Error('Failed to load metadata.json: %s'.format(e.toString()));
    }
    let meta;
    try {
        meta = JSON.parse(metadataContents);
    } catch (e) {
        throw new Error('Failed to parse metadata.json: %s'.format(e.toString()));
    }

    meta.uuid = newUuid;
    metadataContents = JSON.stringify(meta, null, 2);

    try {
        metadataFile.replace_contents(metadataContents, null, false, Gio.FileCreateFlags.NONE, null);
    } catch (e) {
        throw new Error('Failed to replace metadata.json: %s'.format(e.toString()));
    }
}

function installFromDirectory(uuid, dir) {
    try {
        let extension = this.createExtensionObject(uuid, dir, ExtensionUtils.ExtensionType.PER_USER);
        this.loadExtension(extension);
        if (!this.enableExtension(uuid))
            throw new Error('Cannot add %s to enabled extensions gsettings key'.format(uuid));
    } catch (e) {
        let extension = this.lookup(uuid);
        if (extension)
            this.unloadExtension(extension);
        throw new Error('Error while installing %s: %s (%s)'.format(uuid, 'LoadExtensionError', e));
    }
}

// Install a new extension without restarting the shell, as if it was coming from ego. The directory must be in ~/.local/share/gnome-shell/extensions.
function installFromLocal(uuid) {
    let dir = Gio.File.new_for_path(GLib.build_filenamev([global.userdatadir, 'extensions', uuid]));
    this._installFromDirectory(uuid, dir);
}

// Reload an extension without restarting the shell. The directory must be in .local/share/gnome-shell/extensions.
function reloadExtension(uuid) {
    let extension = this.lookup(uuid);
    if (extension)
        this.unloadExtension(extension);

    let dir = Gio.File.new_for_path(GLib.build_filenamev([global.userdatadir, 'extensions', uuid]));
    if (!dir.query_exists(null))
        throw new Error(`${dir.get_path()} not found`);

    // ExtensionUtils.getCurrentExtension expects the new directory path to contain 'gnome-shell/extensions'.
    let tempDir = Gio.File.new_for_path(GLib.build_filenamev([GLib.get_tmp_dir(),'gnome-shell/extensions']));

    if (!this._forbiddenReloadedUuids)
        this._forbiddenReloadedUuids = new Set();

    // Unload previous reloaded extension (if exists).
    [...this._extensions.keys()].filter(key => key.startsWith(MATCH.format(uuid))).forEach(key => {
        this._forbiddenReloadedUuids.add(key);
        if (this.lookup(key) && this.lookup(key).dir.has_parent(tempDir))
            ExtensionDownloader.uninstallExtension(key);
    });

    // Clean settings from previous reloaded extensions (not important).
    let enabledExtensions = global.settings.get_strv(ENABLED_EXTENSIONS_KEY);
    if (enabledExtensions.some(key => key.startsWith(MATCH.format(uuid)))) {
        enabledExtensions = enabledExtensions.filter(key => !key.startsWith(MATCH.format(uuid)));
        global.settings.set_strv(ENABLED_EXTENSIONS_KEY, enabledExtensions);
    }
    let disabledExtensions = global.settings.get_strv(DISABLED_EXTENSIONS_KEY);
    if (disabledExtensions.some(key => key.startsWith(MATCH.format(uuid)))) {
        disabledExtensions = disabledExtensions.filter(key => !key.startsWith(MATCH.format(uuid)));
        global.settings.set_strv(DISABLED_EXTENSIONS_KEY, disabledExtensions);
    }

    // A new uuid for each reload.
    let i = 1;
    while (this._forbiddenReloadedUuids.has(UUID.format(uuid, i)))
        i++;
    let newUuid = UUID.format(uuid, i);

    // Copy extension directory in /tmp and change uuid in the directory name and metadata.json.
    let newDir = tempDir.get_child(newUuid);
    if (newDir.query_exists(null))
        FileUtils.recursivelyDeleteDir(newDir, true);
    try {
        recursivelyCopyDir(dir, newDir);
        changeMetadataUuid(newDir, newUuid);
    } catch(e) {
        try {
            FileUtils.recursivelyDeleteDir(newDir, true);
        } catch(e) {}
        throw e;
    }

    // Load the new extension.
    this._installFromDirectory(newUuid, newDir);

    // no openExtensionPrefs in GS 3.34
    let openExtensionPrefs = (this.openExtensionPrefs || new Function()).bind(this, newUuid, '', {});
    openExtensionPrefs.toString = () => 'openExtensionPrefsFunction';

    return [newUuid, openExtensionPrefs];
}

async function reloadExtensionAsync(uuid) {
    return this.trulyReloadExtension(uuid);
}

class Service {
    constructor() {
        this._dbusImpl = Gio.DBusExportedObject.wrapJSObject(DBUS_INTERFACE, this);
        this._dbusImpl.export(Gio.DBus.session, DBUS_OBJECT_PATH);
    }

    disable() {
        this._dbusImpl.unexport();
    }

    ReloadExtension(uuid) {
        try {
            let newUuid = Main.extensionManager.trulyReloadExtension(uuid)[0];
            return [true, newUuid, ''];
        } catch(e) {
            return [false, '', e.message];
        }
    }
}

class Extension {
    constructor() {
    }

    enable() {
        Main.extensionManager._installFromDirectory = installFromDirectory;
        Main.extensionManager.installFromLocal = installFromLocal;
        Main.extensionManager.trulyReloadExtension = reloadExtension;
        Main.extensionManager.trulyReloadExtensionAsync = reloadExtensionAsync;
        this.service = new Service();
    }

    disable() {
        delete Main.extensionManager._installFromDirectory;
        delete Main.extensionManager.installFromLocal;
        delete Main.extensionManager.trulyReloadExtension;
        delete Main.extensionManager.trulyReloadExtensionAsync;
        this.service.disable();
    }
}

function init() {
    return new Extension();
}
